
import os
import sys
import copy
import types
import shutil
import tempfile
import unittest

from importlib import import_module
from contextlib import closing
from logging import basicConfig, DEBUG

try:
    from urllib.request import urlopen
except ImportError:
    from urllib2 import urlopen

try:
    import numpy
except:
    numpy = object()

PYMSSQL = (
    {
        "mssql": (
            "https://github.com/pymssql/pymssql/releases/download/"
            "2.0.0/pymssql-2.0.0-cp{PYVERSION}-none-{PYARCH}.whl"
        ),
    },
    ("_mssql", "pymssql"),
)

PSUTIL = (
    {
        "psutil": (
            "https://pypi.python.org/packages/cd/b0/"
            "07b7083a134c43b58515d59f271734034f8ba06840b1f371eaa6b3ab85b2"
            "/psutil-4.3.1-cp35-cp35m-win32.whl"
        ),
    },
    ("psutil._psutil_windows", ),
)

PYWIN32 = (
    {
        "pypiwin32": (
            "https://pypi.python.org/packages/5b/68/"
            "436ce631dc0584969d03186d095f4daf09b5c0193ebd66927524a33411c8"
            "/pypiwin32-219-cp35-none-win32.whl"
        ),
    },
    ("pywintypes", ),  # TODO: Unable to load win32service.pyd, win32api, etc
)

PANDAS = (
    {
        "pandas": (
            "https://github.com/pydata/pandas/releases/download/v0.18.1/"
            "pandas-0.18.1-cp{PYVERSION}-cp{PYVERSION}m-{PYARCH}.whl"
        ),
        "pytz": (
            "https://pypi.python.org/packages/ba/c7/"
            "3d54cad4fb6cf7bf375d39771e67680ec779a541c68459210fcfdc3ba952"
            "/pytz-2016.6.1-py2.py3-none-any.whl"
        ),
        "dateutil": (
            "https://pypi.python.org/packages/33/68/"
            "9eadc96f9899caebd98f55f942d6a8f3fb2b8f8e69ba81a0f771269897e9"
            "/python_dateutil-2.5.3-py2.py3-none-any.whl"
        ),
    },
    ("pandas.hashtable", ),
)

_VERSION = sys.version_info[:2]
_ARCH = 64 if sys.maxsize > 2**32 else 32
_NUMPY_AVAILABLE = hasattr(numpy, "__version__")


def download(dest, wheels):
    for name, _url in wheels.items():
        url = _url.format(
            PYVERSION="%d%d" % _VERSION,
            PYARCH="win_amd64" if _ARCH == 64 else "win32",
        )
        with closing(urlopen(url)) as res:
            with open(os.path.join(dest, name + ".whl"), "wb") as fp:
                fp.write(res.read())


class ZipExtensionImporterTest(unittest.TestCase):
    def setUp(self):
        self.temp = tempfile.mkdtemp()
        self.backup_path = copy.copy(sys.path)
        self.backup_path_hooks = copy.copy(sys.path_hooks)
        sys.path_importer_cache.clear()

    def tearDown(self):
        shutil.rmtree(self.temp)
        sys.path[:] = self.backup_path
        sys.path_hooks[:] = self.backup_path_hooks
        sys.path_importer_cache.clear()

    def _basic_check(self, modules):
        zipextimport2 = import_module("zipextimport2")
        ZipExtensionImporter = zipextimport2.ZipExtensionImporter
        for fname in os.listdir(self.temp):
            sys.path.insert(0, os.path.join(self.temp, fname))
        sys.path_hooks.insert(0, ZipExtensionImporter)
        for modname in modules:
            sys.modules.pop(modname, None)
            mod = import_module(modname)
            self.assertTrue(hasattr(mod, "__package__"))
            self.assertIn("zipextimport2", repr(mod.__loader__))

    @unittest.skipUnless(
        (2, 6) <= _VERSION < (3, 5) and _ARCH == 32,
        "Test requires Python 2.6 to 3.4 (32 bit)"
    )
    def test_with_pymssql(self):
        download(self.temp, PYMSSQL[0])
        self._basic_check(PYMSSQL[1])

    @unittest.skipUnless(
        _VERSION == (3, 5) and _ARCH == 32,
        "Test requires Python 3.5 (32 bit)"
    )
    def test_with_pywin32(self):
        download(self.temp, PYWIN32[0])
        self._basic_check(PYWIN32[1])

    @unittest.skipUnless(
        (2, 7) <= _VERSION < (3, 6) and _NUMPY_AVAILABLE,
        "Test requires Python 2.7 to 3.5 with numpy"
    )
    def test_with_pandas(self):
        download(self.temp, PANDAS[0])
        self._basic_check(PANDAS[1])

    @unittest.skipUnless(
        _VERSION == (3, 5) and _ARCH == 32,
        "Test requires Python 3.5 (32 bit)"
    )
    def test_with_psutil(self):
        download(self.temp, PSUTIL[0])
        self._basic_check(PSUTIL[1])


class MemimporterTest(unittest.TestCase):
    def test_import(self):
        import_module("zipextimport2.memimporter")

    def test_with_std_pylibs(self):
        memimporter = import_module("zipextimport2.memimporter")

        base = os.path.join(os.path.dirname(sys.executable), "DLLs")
        modules = [
            "_ctypes",
            "_hashlib",
            "_ssl",
        ]

        for modname in modules:
            pydpath = os.path.join(base, modname + ".pyd")
            with open(pydpath, "rb") as fp:
                mod = memimporter.import_module(modname, fp.read())
                self.assertIsInstance(mod, types.ModuleType)
                self.assertEqual(dir(mod), dir(__import__(modname)))

    def test_with_non_pylibs(self):
        memimporter = import_module("zipextimport2.memimporter")

        base = os.path.dirname(
            os.environ.get("ComSpec", r"C:\Windows\system32\cmd.exe")
        )
        dlls = [
            "user32",
            "kernel32",
        ]
        for dll in dlls:
            dllpath = os.path.join(base, dll + ".dll")
            with open(dllpath, "rb") as fp:
                self.assertRaises(
                    ImportError,
                    memimporter.import_module,
                    dll, fp.read(),
                )


class Zipextimport2Test(unittest.TestCase):
    def test_import(self):
        zipextimport2 = import_module("zipextimport2")
        for export in zipextimport2.__all__:
            self.assertTrue(
                hasattr(zipextimport2, export),
                "%s was not found" % export
            )
        exports = [ex for ex in dir(zipextimport2) if not ex.startswith("_")]
        self.assertEqual(set(exports), set(zipextimport2.__all__))


if __name__ == "__main__":
    if "-v" in sys.argv[1:]:
        basicConfig(level=DEBUG)
        unittest.main(verbosity=2)
    else:
        unittest.main()
