zipextimport2
====

zipextimport2 is aiming to become a replacement of ZipExtImporter.

Dependencies
====

- PyMemoryModule: https://bitbucket.org/sakurai_youhei/pymemorymodule

How to use zipextimport2.ZipExtensionImporter
====

```
import sys
from zipextimport2 import ZipExtensionImporter

sys.path_hooks.insert(0, ZipExtensionImporter)
sys.path.append("path/to/your/package.whl")

from package import your.module_pyd
your.module_pyd.do_anything_you_want()
```

_Debug logging from memimpoter can be suppressed by logging.getLogger('memimporter').setLevel(logging.INFO)._

How to use zipextimport2.memimporter
====

```
from zipextimport2 import memimporter

with open("path/to/your/module.pyd", "rb") as fp:
    mod = memimporter.import_module("your.module", fp.read())
    mod.do_anything_you_want()
```

Comparison between _memimporter and zipextimport2.memimporter
====

_memimporter is tied up with MemoryModule and is all written as C extension but zipextimport2.memimporter is written as Pure Python module using ctypes with delegating lower layer upon thin Python binding named PyMemoryModule. That's why:

- zipextimport2.memimporter is easier to understand what's written on the inside.
- zipextimport2.memimporter has better error handling thanks to wrapping with ctypes.
- zipextimport2.memimporter uses standard logging to inform diagnostic info.

General limitations
====

Still unable to load numpy compiled with C acceleration modules such as BLAS, LAPACK and ATLAS enabled.

Limitations in case of use within py2exe
====

There are following limitations when using zipextimport2 within py2exe 0.9.2.2 on Python 3.4 32 bit at the moment.

1. zipextimport2.memimporter is incompatible with bundle\_files=0 & bundle\_files=1 options in py2exe (bundle\_files=2 option works fine, under the same conditions though). The actual behavior of bundle\_files=0 & bundle\_files=1 is that process terminates with **'Fatal Python error: PyThreadState_Get: no current thread'** maybe because of some problem around at GIL and ctypes in py2exe.
2. Before switching to zipextimport2.ZipExtensionImporter, as a workaround, it is necessary to open at least one zip file using zipfile.ZipFile which module has to be loaded by original zipextimporter.ZipExtensionImporter like below code.

```
import sys
import zipfile

import zipextimporter
import zipextimport2

zipfile.ZipFile(sys.executable, "r").close()  # This is what needs to be done.

try:
    # Removing original ZipExtensionImporter
    sys.path_hooks.remove(zipextimporter.ZipExtensionImporter)
    sys.path_importer_cache.clear()
except ValueError:
    pass

sys.path_hooks.insert(0, zipextimport2.ZipExtensionImporter)
```

How to run code check
====

```
python -m pip install flake8
python -m flake8 --show-source zipextimport2 setup.py test.py
```

ZipExtImporter
====

http://www.py2exe.org/index.cgi/Hacks/ZipExtImporter

License
====

Mozilla Public License Version 2.0 (MPL2.0)