
from os import stat as _os_stat
from zipfile import is_zipfile
from itertools import product
from zipimport import zipimporter, ZipImportError
from importlib.machinery import EXTENSION_SUFFIXES

from zipextimport2.zipextutils import WheelLikeZipFile


class ZipExtensionFinder(object):
    def __init__(self, path, *loader_details):
        """\
        @param loader_details: ((suffix, loader), (suffix, loader), ...)
        """
        self.path = path
        self.archive = zipimporter(self.path).archive  # TODO: find wiser way
        self.loaders = dict(loader_details)
        self._path_mtime = -1
        self._paths_cache = set()
        self._prefixes_cache = set()

        if not is_zipfile(self.archive):
            raise ZipImportError("not zip file, %s" % self.archive)
        else:
            self._fill_caches()
            self._path_mtime = self._get_mtime()

    def _fill_caches(self):
        """\
        Fetch list of names from zip file
        """
        with WheelLikeZipFile(self.archive, "r") as whlzfp:
            paths = set(whlzfp.namelist())

            prefixes = set()
            prefixes |= set(whlzfp.pywin32_prefixes())
            for site_pth in whlzfp.root_site_pths():
                prefixes |= set(
                    whlzfp.site_prefixes(site_pth)
                )

            self._prefixes_cache = prefixes
            self._paths_cache = paths

    def _get_mtime(self):
        try:
            return _os_stat(self.archive).st_mtime
        except OSError:
            return -1

    def find_module(self, fullname, path=None):
        mtime = self._get_mtime()
        if mtime != self._path_mtime:
            self._fill_caches()
            self._path_mtime = mtime

        filebase = fullname.replace(".", "/")
        for suffix, loader in self.loaders.items():
            filepaths = [filebase + suffix]

            if suffix not in EXTENSION_SUFFIXES:
                filepaths.append(filebase + "/__init__" + suffix)

            for prefix, filepath in product(self._prefixes_cache, filepaths):
                filepaths.append(prefix + "/" + filepath)

            if any(map(lambda f: f in self._paths_cache, filepaths)):
                return loader(self.path)

        return None

    @classmethod
    def path_hook(cls, *loader_details):
        def closure(path):
            return cls(path, *loader_details)
        return closure
