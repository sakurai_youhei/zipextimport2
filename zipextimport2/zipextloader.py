
import sys
import types

from os.path import join as _joinpath
from os.path import normpath as _normpath
from zipfile import is_zipfile
from itertools import product
from zipimport import zipimporter, ZipImportError
from importlib.machinery import EXTENSION_SUFFIXES

from zipextimport2.memimporter import import_module
from zipextimport2.zipextutils import WheelLikeZipFile


class ZipExtensionLoader(object):
    suffixes = EXTENSION_SUFFIXES

    def __init__(self, path):
        self.path = path
        self.archive = zipimporter(self.path).archive  # TODO: find wiser way

    def _load_extension(self, fullname, data):
        try:
            return import_module(fullname, data)
        except ImportError as e:
            raise ZipExtensionImportError(e)
        finally:
            sys.modules.pop(fullname, None)

    def load_module(self, fullname):
        if fullname in sys.modules:
            return sys.modules[fullname]
        elif not is_zipfile(self.archive):
            raise ZipImportError("not zip file, %s" % self.archive)

        filebase = fullname.replace(".", "/")
        filepaths = [filebase + suffix for suffix in self.suffixes]

        sys.modules[fullname] = mod = types.ModuleType(fullname)
        with WheelLikeZipFile(self.archive, "r") as whlzfp:
            prefixes = set()
            prefixes |= set(whlzfp.pywin32_prefixes())
            for site_pth in whlzfp.root_site_pths():
                prefixes |= set(
                    whlzfp.site_prefixes(site_pth)
                )

            for prefix, filepath in product(prefixes, filepaths):
                filepaths.append(prefix + "/" + filepath)

            filepaths = filter(lambda f: f in whlzfp.namelist(), filepaths)
            try:
                filepath = next(filepaths)
                mod = self._load_extension(fullname, whlzfp.read(filepath))
                mod.__loader__ = self
                mod.__file__ = _normpath(
                    _joinpath(self.archive, filepath)
                )
                sys.modules[fullname] = mod
                return mod
            except StopIteration:
                sys.modules.pop(fullname, None)
                raise ZipImportError(
                    "%s %s was not found in %s",
                    fullname, self.suffixes, self.archive,
                )


class ZipExtensionDllLoader(ZipExtensionLoader):
    suffixes = (".dll", "%d%d.dll" % sys.version_info[:2], )

    def _load_extension(self, fullname, data):
        try:
            return import_module(fullname, data, False)
        except ImportError as e:
            raise ZipExtensionImportError(e)
        finally:
            sys.modules.pop(fullname, None)


class ZipExtensionImportError(ZipImportError):
    pass
