
from itertools import chain
from itertools import product
from zipimport import zipimporter
from importlib import machinery

from zipextimport2 import memimporter
from zipextimport2.zipextfinder import ZipExtensionFinder
from zipextimport2.zipextloader import ZipExtensionLoader
from zipextimport2.zipextloader import ZipExtensionDllLoader

__all__ = [
    "zipextutils",
    "memimporter",
    "zipextfinder",
    "zipextloader",
    "ZipExtensionImporter",
]

ZipExtensionImporter = ZipExtensionFinder.path_hook(
    *chain(
        product(machinery.BYTECODE_SUFFIXES, [zipimporter]),
        product(machinery.DEBUG_BYTECODE_SUFFIXES, [zipimporter]),
        product(machinery.EXTENSION_SUFFIXES, [ZipExtensionLoader]),
        product(machinery.OPTIMIZED_BYTECODE_SUFFIXES, [zipimporter]),
        product(machinery.SOURCE_SUFFIXES, [zipimporter]),
        product(ZipExtensionDllLoader.suffixes, [ZipExtensionDllLoader]),
    )
)

del chain
del product
del zipimporter
del machinery

assert memimporter
del ZipExtensionFinder
del ZipExtensionLoader
del ZipExtensionDllLoader

assert ZipExtensionImporter
