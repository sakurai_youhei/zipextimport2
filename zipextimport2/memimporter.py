
import sys
import ctypes
import warnings
import functools

from logging import getLogger

import pymemorymodule as pymm

_VERBOSE = 0

_Py_PackageContext = ctypes.c_char_p.in_dll(
    ctypes.pythonapi,
    "_Py_PackageContext"
)


def get_verbose_flag():
    global _VERBOSE
    warnings.warn(
        "This function is kept only for zipextimporter.",
        category=DeprecationWarning,
        stacklevel=2
    )
    return _VERBOSE


def set_verbose_flag(flag):
    global _VERBOSE
    warnings.warn(
        "This function will be deprecated with get_verbose_flag().",
        category=DeprecationWarning,
        stacklevel=2
    )
    _VERBOSE = flag


def _to_initfuncname(modname):
    if sys.version_info[0] >= 3:
        prefix = "PyInit_"
    else:
        prefix = "init"
    return prefix + modname.split(".")[-1]


def import_module(*args):
    """\
    Returns module w/ setting only __file__ and w/o adding to sys.modules.

    @param args:
        (modname, pathname, initfuncname, findproc) -> legacy interface
        (modname, data, switch_context=True) -> new interface added in zipextimport2
    """
    if len(args) == 4:
        warnings.warn(
            (
                "Legacy interface is used. Please consider to move to "
                "modern interface which is 'import_module(modname , data)'"
            ),
            category=DeprecationWarning,
            stacklevel=2
        )
        modname, pathname, initfuncname, findproc = args
        data = findproc(pathname)
        switch_context = False
    elif len(args) == 2 or len(args) == 3:
        modname, data, switch_context = (args + (True, ))[:3]
        initfuncname = _to_initfuncname(modname)
    else:
        raise TypeError(
            "Invalid number of arguments (%s given)" % len(args)
        )
    return _import_module(modname, data, initfuncname, switch_context)


@functools.lru_cache(maxsize=None)
def _import_module(modname, data, initfuncname, switch_context):
    logger = getLogger(__name__.split(".")[-1])
    logger.debug("Starting to import %s", modname)

    try:
        logger.debug("Loading DLL/EXE using %s bytes data", len(data))
        handle = pymm.MemoryLoadLibrary(data)

        logger.debug("Resolving address of %s", initfuncname)
        p_initfunc = pymm.MemoryGetProcAddress(handle, initfuncname)
        initfunc = ctypes.cast(p_initfunc, ctypes.PYFUNCTYPE(ctypes.py_object))

        if switch_context:
            logger.debug("Switching _Py_PackageContext to %s", modname)
            old_context = _Py_PackageContext.value
            _Py_PackageContext.value = modname.encode("ascii")

        logger.debug("Initializing %s", modname)
        mod = initfunc()

        if "old_context" in locals():
            logger.debug("Restoring _Py_PackageContext to %s", old_context)
            _Py_PackageContext.value = old_context

        setattr(mod, "__file__", repr(handle))
        logger.debug("Initialized %r", mod)
        return mod
    except Exception as e:
        logger.debug("Failed to import %s, %r" % (modname, e))
        _free_memory_if_necessary(**locals())
        raise ImportError(e)


def _free_memory_if_necessary(**kwargs):
    logger = kwargs["logger"]
    if "handle" in kwargs:
        if "initfunc" not in kwargs:
            try:
                logger.debug("Freeing loaded DLL/EXE")
                pymm.MemoryFreeLibrary(kwargs["handle"])
            except (TypeError, OSError):
                logger.warning("Failed to free %r", kwargs["handle"])
        else:
            logger.debug("Not freeing loaded DLL/EXE")
