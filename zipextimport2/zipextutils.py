
import os

from zipfile import ZipFile


class WheelLikeZipFile(ZipFile):
    def site_pths(self):
        if hasattr(self, "namelist"):
            for name in self.namelist():
                if name.endswith(".pth"):
                    yield name

    def root_site_pths(self):
        for site_pth in self.site_pths():
            if os.path.dirname(site_pth) == "":
                yield site_pth

    def pywin32_prefixes(self):
        if hasattr(self, "namelist"):
            proba = filter(lambda f: "win32_system32/" in f, self.namelist())
            for prefix in set(map(lambda f: f.split("/")[0], proba)):
                if os.path.dirname(prefix) == "":
                    yield prefix

    def site_prefixes(self, site_pth, pwd=None):
        content = self.read(site_pth, pwd).decode()
        for line in content.splitlines():
            line = line.strip()
            if not line or line.startswith("#"):
                continue  # empty or comment line
            elif line.startswith("import ") or line.startswith("import\t"):
                pass  # TODO: might need to execute this line
            else:
                yield line
