from distutils.core import setup

setup(
    version='0.0.1',
    name="zipextimport2",
    license="MPL2.0",
    description=(
        "zipextimport2 is aiming to become a replacement of ZipExtImporter."
    ),
    author="Youhei Sakurai",
    author_email="sakurai.youhei@gmail.com",
    packages=["zipextimport2"],
    requires=["pymemorymodule"],
)
